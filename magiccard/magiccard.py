#!/usr/bin/python3

import requests
import json

url = 'https://api.magicthegathering.io/v1/cards?page=2&set=P02'

def main():

    cardresp = requests.get(url)
    cardresp = cardresp.json()

    toughcards = []
    print("\nCreatures with toughness 1 are:\n")
    for card in cardresp["cards"]:
        if card.get("toughness") == "1":
            print(card.get("name"))
            toughcards.append({"name":card.get("name"),"toughness":card.get("toughness"),"power":card.get("power"),"flavor":card.get("flavor")})

    print(f"\nTotal number of cards is {len(toughcards)} \n")

if __name__ == "__main__":
    main()

